package fr.uavignon.ceri.projet;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.fragment.NavHostFragment;

import com.bumptech.glide.Glide;
import com.google.android.material.snackbar.Snackbar;

public class DetailFragment extends Fragment {
    public static final String TAG = DetailFragment.class.getSimpleName();

    private DetailViewModel viewModel;
    private TextView textName, textDescription, textYear, textBrand, textWorking, textCategories;
    private ImageView imgArtefact;
    private ProgressBar progress;


    @Override
    public View onCreateView(
            LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState
    ) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_detail, container, false);
    }

    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        viewModel = new ViewModelProvider(this).get(DetailViewModel.class);

        // Get selected artefact
        DetailFragmentArgs args = DetailFragmentArgs.fromBundle(getArguments());
        String artefactID = args.getItemNum();
        Log.d(TAG,"SELECTED ID = "+artefactID);
        viewModel.setArtefact(artefactID);

        listenerSetup();
        observerSetup();

    }


    private void listenerSetup() {
        textName = getView().findViewById(R.id.artefactNom);
        textDescription = getView().findViewById(R.id.artefactDescription);
        textYear = getView().findViewById(R.id.artefactAnnee);
        textBrand = getView().findViewById(R.id.artefactMarque);
        textWorking = getView().findViewById(R.id.artefactEtat);
        textCategories = getView().findViewById(R.id.artefactCategories);

        imgArtefact = getView().findViewById(R.id.artefactImg);

        progress = getView().findViewById(R.id.progress);


        getView().findViewById(R.id.buttonBack).setOnClickListener(view -> NavHostFragment.findNavController(DetailFragment.this)
                .navigate(R.id.action_DetailFragment_to_ListFragment));
    }

    private void observerSetup() {
        viewModel.getArtefact().observe(getViewLifecycleOwner(),
                artefact -> {
                    if (artefact != null) {
                        Log.d(TAG, "Observing artefact view");

                        if(artefact.getName() != null) {
                            textName.setText(artefact.getName());
                        }
                        else{
                            textName.setText("Nom inconnu");
                        }

                        if(artefact.getDescription() != null){
                            textDescription.setText(artefact.getDescription());
                        }
                        else{
                            textDescription.setText("Description inconnue");
                        }

                        if(artefact.getBrand() != null){
                            textBrand.setText(artefact.getBrand());
                        }
                        else{
                            textBrand.setText("Marque inconnue");
                        }

                        if(artefact.getYear() > 0){
                            textYear.setText(String.valueOf(artefact.getYear()));
                        }
                        else{
                            textYear.setText("Année inconnue");
                        }

                        if(artefact.getWorking() == true){
                            textWorking.setText("Fonctionnel");
                        }
                        else if(artefact.getWorking() == false){
                            textWorking.setText("Non fonctionnel");
                        }
                        else{
                            textWorking.setText("Etat inconnu");
                        }

                        if(artefact.getCategories() != null){
                            textCategories.setText((CharSequence) artefact.getCategories());
                        }
                        else{
                            textCategories.setText("Aucune catégorie");
                        }
                        if(artefact.getPicture() != null)
                        {
                            Glide.with(this)
                                    .load(artefact.getPicture())
                                    .into(imgArtefact);
                        }
                    }
                });

    viewModel.getIsLoading().observe(getViewLifecycleOwner(),
            isLoading ->{
                if(isLoading){
                    progress.setVisibility(View.VISIBLE);
                }
                else{
                    progress.setVisibility(View.GONE);
                }
            }
            );
    viewModel.getWebServiceThrowable().observe(getViewLifecycleOwner(),
                throwable ->{
                    Snackbar snackbar = Snackbar
                            .make(getView(), throwable.getMessage(), Snackbar.LENGTH_LONG);
                    snackbar.show();
                }
        );
    }
}