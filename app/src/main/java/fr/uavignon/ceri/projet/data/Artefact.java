package fr.uavignon.ceri.projet.data;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.Index;
import androidx.room.PrimaryKey;


@Entity(tableName = "artefact_database", indices = {@Index(value = {"name", "description"},
        unique = true)})
public class Artefact {

    @PrimaryKey
    @NonNull
    @ColumnInfo(name="_id")
    private String id;

    @NonNull
    @ColumnInfo(name="name")
    private String name;

    @NonNull
    @ColumnInfo(name="description")
    private String description=null;

    @NonNull
    @ColumnInfo(name="brand")
    private String brand;

    @NonNull
    @ColumnInfo(name="working")
    private boolean working=false;

    @NonNull
    @ColumnInfo(name="year")
    private int year=-1;

    @NonNull
    @ColumnInfo(name="categories")
    private String categories = null;

    @ColumnInfo(name="picture")
    private String picture = null;

    public Artefact() { }


    //Getters
    public String getName() { return name; }

    public String getId() { return id; }

    public int getYear() { return year; }

    public String getDescription() { return description; }

    @NonNull
    public String getCategories() { return categories; }

    public boolean getWorking() { return working; }

    public void setYear(int year) { this.year = year; }

    @NonNull
    public String getBrand() { return brand; }

    public String getPicture() { return picture; }

    //Setters
    public void setName(String name) { this.name = name; }

    public void setId(String id) { this.id = id; }

    public void setCategories(@NonNull String categories) { this.categories = categories; }

    public void setWorking(Boolean working) { this.working = working; }

    public void setDescription(String description) { this.description = description; }

    public void setBrand(String brand) { this.brand=brand; }

    public void setPicture(String picture) { this.picture = picture; }
}
