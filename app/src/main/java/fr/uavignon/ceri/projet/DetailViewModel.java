package fr.uavignon.ceri.projet;

import android.app.Application;

import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import fr.uavignon.ceri.projet.data.Artefact;
import fr.uavignon.ceri.projet.data.MuseeRepository;

public class DetailViewModel extends AndroidViewModel {
    private MutableLiveData<Boolean> isLoading;
    private MuseeRepository repository;
    private MutableLiveData<Artefact> artefact;
    private  MutableLiveData<Throwable> webServiceThrowable;


    public DetailViewModel (Application application) {
        super(application);
        isLoading = new MutableLiveData<>();
        repository = MuseeRepository.get(application);
        isLoading = repository.isLoading;
        webServiceThrowable = repository.webServiceThrowable;
        artefact = new MutableLiveData<>();
    }

    public void setArtefact(String id) {
        repository.getArtefact(id);
        artefact = repository.getSelectedArtefact();
    }
    LiveData<Artefact> getArtefact() {
        return artefact;
    }

    LiveData<Boolean> getIsLoading() {
        return isLoading;
    }

    LiveData<Throwable> getWebServiceThrowable(){return webServiceThrowable;}


}

