package fr.uavignon.ceri.projet;


import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.util.Log;
import android.view.ActionMode;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.RecyclerView;
import com.bumptech.glide.Glide;

import com.google.android.material.snackbar.Snackbar;

import java.util.ArrayList;

import fr.uavignon.ceri.projet.data.Artefact;

public class RecyclerAdapter extends RecyclerView.Adapter<fr.uavignon.ceri.projet.RecyclerAdapter.ViewHolder> {

    private static final String TAG = fr.uavignon.ceri.projet.RecyclerAdapter.class.getSimpleName();
    private ListViewModel listViewModel;

    private static int cpt_alterne = 0;
    private ArrayList<Artefact> artefactList;

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {

        View view;
        if(cpt_alterne % 2 == 0) {
            view = LayoutInflater.from(viewGroup.getContext())
                    .inflate(R.layout.card_layout_gauche, viewGroup, false);
        }
        else{
            view = LayoutInflater.from(viewGroup.getContext())
                    .inflate(R.layout.card_layout_droite, viewGroup, false);
        }
        cpt_alterne++;
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i) {
        viewHolder.itemTitle.setText(artefactList.get(i).getName());

        if(artefactList.get(i).getYear() > 0){
            viewHolder.itemDetail.setText("Date de sortie : " + String.valueOf(artefactList.get(i).getYear()));
        }
        else{
            viewHolder.itemDetail.setText("Année inconnue");
        }

        // Insertion Image
        if (artefactList.get(i).getPicture() != null)
        {
            Glide.with(viewHolder.itemView.getContext())
                    .load(artefactList.get(i).getPicture())
                    .into(viewHolder.imgArtefact);
        }

        if (artefactList.get(i).getCategories() != null)
        {
            viewHolder.itemCategories.setText(artefactList.get(i).getCategories());
        }
        else{
            viewHolder.itemCategories.setText("Aucune catégorie");
        }
    }

    @Override
    public int getItemCount() {
        return artefactList == null ? 0 : artefactList.size();
    }

    public void setListViewModel(ListViewModel viewModel) {
        listViewModel = viewModel;
    }

    private void deleteItem(String id) {
        if (listViewModel != null) {
            listViewModel.deleteArtefact(id);
        }
    }

    public void setArtefactsList(ArrayList<Artefact> artefacts) {
        artefactList = artefacts;
        notifyDataSetChanged();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        TextView itemTitle;
        TextView itemDetail;
        ImageView imgArtefact;
        TextView itemCategories;

         ActionMode actionMode;
        String idSelectedLongClick;

        ViewHolder(View itemView) {
            super(itemView);
            itemTitle = itemView.findViewById(R.id.item_title);
            itemDetail = itemView.findViewById(R.id.item_detail);
            imgArtefact = itemView.findViewById(R.id.item_image);
            itemCategories = itemView.findViewById(R.id.item_categories);

            ActionMode.Callback actionModeCallback = new ActionMode.Callback() {

                @Override
                public boolean onCreateActionMode(ActionMode mode, Menu menu) {
                    MenuInflater inflater = mode.getMenuInflater();
                    inflater.inflate(R.menu.context_menu, menu);
                    return true;
                }

                @Override
                public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
                    return false;
                }

                @Override
                public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
                    switch (item.getItemId()) {
                        case R.id.menu_delete:
                            fr.uavignon.ceri.projet.RecyclerAdapter.this.deleteItem(idSelectedLongClick);
                            Snackbar.make(itemView, "Artefact supprimé !",
                                    Snackbar.LENGTH_LONG)
                                    .setAction("Action", null).show();
                            mode.finish();
                            return true;
                        case R.id.menu_update:
                            return true;
                        default:
                            return false;
                    }
                }

                // Called when the user exits the action mode
                @Override
                public void onDestroyActionMode(ActionMode mode) {
                    actionMode = null;
                }
            };

            itemView.setOnClickListener((View.OnClickListener) v -> {
                Log.d(TAG,"position="+getAdapterPosition());
                String id = RecyclerAdapter.this.artefactList.get((int)getAdapterPosition()).getId();
                Log.d(TAG,"id="+id);

                ListFragmentDirections  .ActionListFragmentToDetailFragment action = ListFragmentDirections.actionListFragmentToDetailFragment(id);
                action.setItemNum(id);
                Navigation.findNavController(v).navigate(action);
            });


            itemView.setOnLongClickListener(new View.OnLongClickListener() {

                @Override
                public boolean onLongClick(View v) {
                    idSelectedLongClick = RecyclerAdapter.this.artefactList.get((int)getAdapterPosition()).getId();
                    if (actionMode != null) {
                        return false;
                    }
                    Context context = v.getContext();
                    actionMode = ((Activity)context).startActionMode(actionModeCallback);
                    v.setSelected(true);
                    return true;
                }
            });
        }
     }
}