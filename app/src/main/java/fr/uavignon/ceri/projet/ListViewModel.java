package fr.uavignon.ceri.projet;

import android.app.Application;

import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import java.util.ArrayList;

import fr.uavignon.ceri.projet.data.Artefact;
import fr.uavignon.ceri.projet.data.MuseeRepository;

public class ListViewModel extends AndroidViewModel {
    private MuseeRepository repository;
    private MutableLiveData<Boolean> isLoading;

    private MutableLiveData<ArrayList<Artefact>> allArtefacts;

    public ListViewModel (Application application) {
        super(application);
        allArtefacts = new MutableLiveData<>();
        repository = MuseeRepository.get(application);
        isLoading = repository.isLoading;
        allArtefacts = repository.getArtefacts();
    }


    public void deleteArtefact(String id) {
        repository.deleteArtefact(id);
    }

    LiveData<Boolean> getIsLoading() {
        return isLoading;
    }

    LiveData<ArrayList<Artefact>> getAllArtefacts() {
        return allArtefacts;
    }

    public void loadCollection(){
        Thread t = new Thread(){
            public void run(){
                repository.loadCollection();
            }
        };
        t.start();
    }

    public void loadCollectionFromDatabase(){
        Thread t = new Thread(){
            public void run(){
                repository.loadCollectionFromDatabase();
            }
        };
        t.start();
    }

    public void removeAll(){
        Thread t = new Thread(){
            public void run(){
                repository.removeAll();
            }
        };
        t.start();
    }

    //OrderByName
    public void getArtefactsByName(){
        Thread t = new Thread(){
            public void run(){
                repository.getArtefactsByName();
            }
        };
        t.start();
    }

    //OrderByYear
    public void getArtefactsByYear(){
        Thread t = new Thread(){
            public void run(){
                repository.getArtefactsByYear();
            }
        };
        t.start();
    }
}
