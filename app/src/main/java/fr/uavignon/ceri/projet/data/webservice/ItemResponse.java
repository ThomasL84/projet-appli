package fr.uavignon.ceri.projet.data.webservice;

import java.util.List;

public class ItemResponse {
    public final Boolean working=false;
    public final String description=null;
    public final String name=null;
    public final Integer year =null;
    public final String brand =null;

    public final List<String> categories=null;
}
