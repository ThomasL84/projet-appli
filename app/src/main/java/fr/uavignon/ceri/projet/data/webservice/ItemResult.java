package fr.uavignon.ceri.projet.data.webservice;

import java.util.ArrayList;

import fr.uavignon.ceri.projet.data.Artefact;

public class ItemResult {


    public static void transferInfo(ItemResponse body, String id,ArrayList<Artefact> artefacts) {

        Artefact artefact = new Artefact();

        artefact.setId(id);
        artefact.setName(body.name);
        artefact.setDescription(body.description);
        artefact.setWorking(body.working);

        if(body.year != null){
            artefact.setYear(body.year);
        }
        if(body.brand != null){
            artefact.setBrand(body.brand);
        }
        if(body.categories != null) {
            String categories = body.categories.get(0);
            for(int i = 1; i<body.categories.size(); i++)
            {
                categories = categories + " ; " + body.categories.get(i);
            }
            artefact.setCategories(categories);
        }

        artefact.setPicture("https://demo-lia.univ-avignon.fr/cerimuseum/items/" + id + "/thumbnail");
        artefacts.add(artefact);
    }
}
