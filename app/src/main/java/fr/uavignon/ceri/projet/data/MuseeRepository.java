package fr.uavignon.ceri.projet.data;

import android.app.Application;
import android.util.Log;

import androidx.lifecycle.MutableLiveData;

import java.util.ArrayList;
import java.util.Map;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

import fr.uavignon.ceri.projet.data.database.ArtefactDao;
import fr.uavignon.ceri.projet.data.database.ArtefactRoomDatabase;
import fr.uavignon.ceri.projet.data.webservice.ItemResponse;
import fr.uavignon.ceri.projet.data.webservice.MuseeInterface;
import fr.uavignon.ceri.projet.data.webservice.ItemResult;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.moshi.MoshiConverterFactory;

import static fr.uavignon.ceri.projet.data.database.ArtefactRoomDatabase.databaseWriteExecutor;

public class MuseeRepository {

    private static final String TAG = MuseeRepository.class.getSimpleName();

    private MutableLiveData<Artefact> selectedArtefact =new MutableLiveData<>();

    private final MuseeInterface api;

    public MutableLiveData<Boolean> isLoading = new MutableLiveData<>();
    public MutableLiveData<Throwable> webServiceThrowable = new MutableLiveData<>();

    private ArtefactDao artefactDao;


    volatile int nbAPILoads = 0;


    private MutableLiveData<ArrayList<Artefact>> artefacts = new MutableLiveData<>();

    public MutableLiveData<ArrayList<Artefact>> getArtefacts() {
        return artefacts;
    }

    private static volatile MuseeRepository INSTANCE;

    public synchronized static MuseeRepository get(Application application) {
        if (INSTANCE == null) {
            INSTANCE = new MuseeRepository(application);
        }
        return INSTANCE;
    }

    public MuseeRepository(Application application) {
        ArtefactRoomDatabase db = ArtefactRoomDatabase.getDatabase(application);
        artefactDao = db.ArtefactDao();

        Retrofit retrofit =
                new Retrofit.Builder()
                        .baseUrl("https://demo-lia.univ-avignon.fr/cerimuseum/")
                        .addConverterFactory(MoshiConverterFactory.create())
                        .build();
        api = retrofit.create(MuseeInterface.class);
    }




    public MutableLiveData<Artefact> getSelectedArtefact() {
        return selectedArtefact;
    }



    public void removeAll(){
        artefactDao.deleteAll();
        artefacts.postValue(null);
    }


    public void loadCollectionFromDatabase(){
        ArrayList<Artefact> allArtefacts = (ArrayList<Artefact>) artefactDao.getArtefactByName();
        artefacts.postValue(allArtefacts);
    }

    public void loadCollection(){
        isLoading.postValue(Boolean.TRUE);


        api.getCollection().enqueue(
                new Callback<Map<String, ItemResponse>>() {
                    @Override
                    public void onResponse(Call<Map<String, ItemResponse>> call,
                                           Response<Map<String, ItemResponse>> response) {
                        Log.d("API RESPONSE",response.body().toString());
                        ArrayList<Artefact> artefactTMP = new ArrayList<>();
                        int i = 0;

                        for (String key: response.body().keySet()) {
                            System.out.println(key + "=" + response.body().get(key).name);
                            ItemResult.transferInfo(response.body().get(key), key, artefactTMP);
                            System.out.println(artefactTMP.get(i).getWorking());
                            long res= insertArtefact(artefactTMP.get(i));
                            System.out.println("INSERTION:"+res);
                            i++;
                        }

                        artefacts.setValue(artefactTMP);
                        isLoading.postValue(Boolean.FALSE);
                    }

                    @Override
                    public void onFailure(Call<Map<String, ItemResponse>> call, Throwable t) {
                        Log.d("FAILURE API",t.getMessage());
                        if (nbAPILoads <= 1){
                            isLoading.postValue(Boolean.FALSE);
                        }
                        else{
                            nbAPILoads--;
                        }
                        webServiceThrowable.postValue(t);
                    }
                });

    }


    public long insertArtefact(Artefact newArtefact) {
        Future<Long> flong = databaseWriteExecutor.submit(() -> {
            return artefactDao.insert(newArtefact);
        });
        long res = -1;
        try {
            res = flong.get();
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return res;
    }


    public void deleteArtefact(String id) {
        databaseWriteExecutor.execute(() -> { artefactDao.deleteArtefact(id); });
    }

    public void getArtefact(String id)  {

        Future<Artefact> future_artefact = databaseWriteExecutor.submit(() -> {
            return artefactDao.getArtefactById(id);
        });
        try {
            selectedArtefact.setValue(future_artefact.get());
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public void getArtefactsByName(){
        ArrayList<Artefact> allArtefacts = (ArrayList<Artefact>) artefactDao.getArtefactByName();
        artefacts.postValue(allArtefacts);
    }

    public void getArtefactsByYear(){
        ArrayList<Artefact> allArtefacts = (ArrayList<Artefact>) artefactDao.getArtefactByYear();
        artefacts.postValue(allArtefacts);
    }
}
