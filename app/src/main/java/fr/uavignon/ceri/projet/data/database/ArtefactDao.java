package fr.uavignon.ceri.projet.data.database;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Update;

import java.util.List;

import fr.uavignon.ceri.projet.data.Artefact;

@Dao
public interface ArtefactDao {

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    long insert(Artefact artefact);

    @Query("DELETE FROM artefact_database")
    void deleteAll();

    @Query("SELECT * FROM artefact_database ORDER BY name ASC")
    List<Artefact> getArtefactByName();

    @Query("DELETE FROM artefact_database WHERE _id = :id")
    void deleteArtefact(String id);

    @Query("SELECT * FROM artefact_database WHERE _id = :id")
    Artefact getArtefactById(String id);

    @Update(onConflict = OnConflictStrategy.IGNORE)
    int update(Artefact artefact);

    @Query("SELECT * FROM artefact_database ORDER BY year DESC")
    List<Artefact> getArtefactByYear();
}
